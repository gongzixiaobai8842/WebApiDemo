﻿namespace WebApiDemo
{
    /// <summary>
    /// 响应数据
    /// </summary>
    /// <typeparam name="T">自定义响应的内容</typeparam>
    public class ResponseApi<T>
    {
        /// <summary>
        /// 错误代码
        /// </summary>
        public int code { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string msg { get; set; }
        /// <summary>
        /// 响应数据
        /// </summary>
        public T data { get; set; }

    }
}