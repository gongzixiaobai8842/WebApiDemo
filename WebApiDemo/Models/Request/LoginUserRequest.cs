﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDemo.Models.Response
{
    public class LoginUserRequest
    {
        /// <summary>
        /// 登录手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 图片验证码
        /// </summary>
        public string Code { get; set; }
    }
}